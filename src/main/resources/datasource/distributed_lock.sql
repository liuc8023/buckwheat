DROP TABLE IF EXISTS `distributed_lock`;
CREATE TABLE `distributed_lock`  (
     `lock_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分布式锁名称(唯一主键)',
     `holder` varchar(180) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '持有节点(每个节点自动生成的uuid)',
     `end_time` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '锁的结束时间',
     `last_updated_stamp` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
     `last_updated_tx_stamp` datetime NULL DEFAULT NULL COMMENT '最后更新事务时间',
     `created_stamp` datetime NULL DEFAULT NULL COMMENT '创建时间',
     `created_tx_stamp` datetime NULL DEFAULT NULL COMMENT '创建事务时间',
     PRIMARY KEY (`lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分布式锁表' ROW_FORMAT = DYNAMIC;
