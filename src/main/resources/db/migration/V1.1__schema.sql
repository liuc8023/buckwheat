DROP TABLE IF EXISTS `cust_code_value`;
CREATE TABLE `cust_code_value`  (
    `code_type_id_key` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '码值小类key',
    `code_type_id_value` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '码值小类value',
    `code_type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '码值小类中文说明',
    `code_type_key` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '码值大类key',
    `code_type_key_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '码值大类value',
    `code_type_parent_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '码值小类key父id',
    `code_type_level` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '层级',
    `validate_flage` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '验证标识',
    `attr_value` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性值',
    `attr_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性描述',
    `last_updated_stamp` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
    `last_updated_tx_stamp` datetime NULL DEFAULT NULL COMMENT '最后更新事务时间',
    `created_stamp` datetime NULL DEFAULT NULL COMMENT '创建时间',
    `created_tx_stamp` datetime NULL DEFAULT NULL COMMENT '创建事务时间',
    PRIMARY KEY (`code_type_id_key`, `code_type_key`) USING BTREE
    ) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '码值表' ROW_FORMAT = DYNAMIC;
