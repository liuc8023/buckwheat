package com.buckwheat.jedis;

import redis.clients.jedis.Jedis;

/**
 * @author liuc
 * @version V1.0
 * @date 2022/2/28 21:33
 * @since JDK1.8
 */
public class JedisDemo1 {
    public static void main(String[] args) {
        //创建Jedis对象
//        Jedis jedis = new Jedis("127.0.0.1",6379);
        Jedis jedis = new Jedis("192.168.8.128",6379);
        jedis.auth("123456");
        //测试
        String value = jedis.ping();
        System.out.println(value);
        jedis.set("name","liuc");
        System.out.println(jedis.get("name"));
        jedis.append("name"," is my love!");
        System.out.println(jedis.get("name"));
        //删除某个键
        jedis.del("name");
        System.out.println(jedis.get("name"));
        jedis.mset("name","liuling","age","23","qq","1010078424");
        System.out.println(jedis.mget("name","age","qq"));
        //进行加1操作
        jedis.incr("age");
        System.out.println(jedis.mget("name","age","qq"));
    }
}
