package com.buckwheat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import java.util.List;


/**
 * 加载启动项参数
 * @author liuc
 * @version V1.0
 * @date 2021/11/12 17:27
 * @since JDK1.8
 */
public class LoadArguments {
    @Autowired
    public LoadArguments(ApplicationArguments applicationArguments) {
        boolean isHaveSkip = applicationArguments.containsOption("skip");
        System.out.println("skip：" + isHaveSkip);
        System.out.println(applicationArguments.getOptionValues("skip"));
        List<String> arguments = applicationArguments.getNonOptionArgs();
        for (int i = 0; i < arguments.size(); i++) {
            System.out.println("非启动项参数：" + arguments.get(i));
        }
    }
}
