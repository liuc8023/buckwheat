package com.buckwheat.module.quartz.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.buckwheat.common.annotation.InitDataSource;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author liuc
 * @date 2021/12/11 14:46
 * @since JDK1.8
 * @version V1.0
 */
@TableName("schedule_job")
@ApiModel(value="ScheduleJob对象",description="定时任务表")
@InitDataSource({"schedule_job"})
public class ScheduleJob implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name="id",example="")
    private Integer id;

    /**
     * 任务名称
     */
    @TableField(value = "job_name")
    @ApiModelProperty(value="任务名称",name="jobName",example="")
    private String jobName;

    /**
     * cron表达式
     */
    @TableField(value = "cron_expression")
    @ApiModelProperty(value="cron表达式",name="cronExpression",example="")
    private String cronExpression;

    /**
     * 服务名称
     */
    @TableField(value = "bean_name")
    @ApiModelProperty(value="服务名称",name="beanName",example="")
    private String beanName;

    /**
     * 方法名称
     */
    @TableField(value = "method_name")
    @ApiModelProperty(value="方法名称",name="methodName",example="")
    private String methodName;

    /**
     * 状态 1.启动 2.暂停
     */
    @TableField(value = "status")
    @ApiModelProperty(value="状态 1.启动 2.暂停",name="status",example="")
    private int status;

    /**
     * 是否删除 0.否 1.是
     */
    @TableField(value="delete_flag")
    @TableLogic
    @ApiModelProperty(value="是否删除 0.否 1.是",name="deleteFlag",example="")
    private Boolean deleteFlag;

    /**
     * 创建人id
     */
    @TableField(value="creator_id")
    @ApiModelProperty(value="创建人id",name="creatorId",example="")
    private Integer creatorId;

    /**
     * 创建人
     */
    @TableField(value="creator_name")
    @ApiModelProperty(value="创建人",name="creatorName",example="")
    private String creatorName;

    /**
     * 创建时间
     */
    @TableField(value = "created_time",fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建时间",name="createdTime",example="")
    private LocalDateTime createdTime;

    /**
     * 更新时间
     */
    @TableField(value = "updated_time",fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新时间",name="updatedTime",example="")
    private LocalDateTime updatedTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(LocalDateTime updatedTime) {
        this.updatedTime = updatedTime;
    }
}
