package com.buckwheat.module.quartz.factory;

import com.buckwheat.common.util.JsonUtil;
import com.buckwheat.common.util.SpringContextUtil;
import com.buckwheat.module.quartz.entity.ScheduleJob;
import lombok.extern.log4j.Log4j2;
import org.quartz.*;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;

/**
 * 
 * @author liuc
 * @date 2021/12/11 23:55
 * @since JDK1.8
 * @version V1.0
 */
/**
 * 当上一个任务未结束时下一个任务需进行等待
 */
@DisallowConcurrentExecution
@Component
@Log4j2
public class QuartzFactory implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        //获取调度数据
        ScheduleJob scheduleJob = (ScheduleJob) jobDataMap.get("scheduleJob");

        //获取对应的Bean
        Object object = SpringContextUtil.getBean(scheduleJob.getBeanName());
        try {
            //利用反射执行对应方法
            Method method = object.getClass().getMethod(scheduleJob.getMethodName());
            method.invoke(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
