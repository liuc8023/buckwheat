package com.buckwheat.module.quartz.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.buckwheat.module.quartz.entity.ScheduleJob;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author liuc
 * @date 2021/12/11 14:47
 * @since JDK1.8
 * @version V1.0
 */
@Mapper
public interface ScheduleJobMapper extends BaseMapper<ScheduleJob> {

}
