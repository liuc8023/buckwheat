package com.buckwheat.module.quartz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.buckwheat.module.quartz.entity.ScheduleJob;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lanjerry
 * @since 2019-01-28
 */
public interface ScheduleJobService extends IService<ScheduleJob> {

    /**
     * 新增定时任务
     * @param job 任务
     * @return void
     * @author liuc
     * @date 2021/12/11 23:58
     * @throws
     */
    void add(ScheduleJob job);

    /**
     * 启动定时任务
     * @param id 任务id
     * @return void
     * @author liuc
     * @date 2021/12/11 23:58
     * @throws
     */
    void start(int id);

    /**
     * 暂停定时任务
     * @param id 任务id
     * @return void
     * @author liuc
     * @date 2021/12/11 23:58
     * @throws
     */
    void pause(int id);

    /**
     * 删除定时任务
     * @param id 任务id
     * @return void
     * @author liuc
     * @date 2021/12/11 23:58
     * @throws
     */
    void delete(int id);

    /**
     * 启动所有定时任务
     * @param
     * @return void
     * @author liuc
     * @date 2021/12/11 23:59
     * @throws
     */
    void startAllJob();

    /**
     * 暂停所有定时任务
     * @param
     * @return void
     * @author liuc
     * @date 2021/12/11 23:59
     * @throws
     */
    void pauseAllJob();
}
