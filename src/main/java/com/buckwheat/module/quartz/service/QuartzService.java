package com.buckwheat.module.quartz.service;

import com.buckwheat.module.quartz.entity.ScheduleJob;
import com.buckwheat.module.quartz.enums.JobOperateEnum;
import org.quartz.SchedulerException;

public interface QuartzService {

    /**
     * 服务器启动执行定时任务
     * @param
     * @return void
     * @author liuc
     * @date 2021/12/11 23:56
     * @throws
     */
    void timingTask();

    /**
     * 新增定时任务
     * @param job
     * @return void
     * @author liuc
     * @date 2021/12/11 23:56
     * @throws
     */
    void addJob(ScheduleJob job);

    /**
     * 操作定时任务
     * @param jobOperateEnum 操作枚举
     * @param job 任务
     * @return void
     * @author liuc
     * @date 2021/12/11 23:56
     * @throws
     */
    void operateJob(JobOperateEnum jobOperateEnum, ScheduleJob job) throws SchedulerException;

    /**
     * 启动所有任务
     * @param
     * @return void
     * @author liuc
     * @date 2021/12/11 23:57
     * @throws
     */
    void startAllJob() throws SchedulerException;

    /**
     * 暂停所有任务
     * @param
     * @return void
     * @author liuc
     * @date 2021/12/11 23:57
     * @throws
     */
    void pauseAllJob() throws SchedulerException;
}
