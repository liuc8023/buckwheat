//package com.buckwheat.module.common.lock;
//
//import com.buckwheat.module.common.factory.YamlAndPropertySourceFactory;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//
///**
// * 分布式事务锁
// * @author liuc
// */
////@PropertySource(value = { "classpath:application-dev.yml" }, encoding="UTF-8",factory = YamlAndPropertySourceFactory.class)
//@Configuration
//@ConfigurationProperties(prefix = "distributed.lock")
//public class DistributedLockUtil {
//	private static final Logger logger = LoggerFactory.getLogger(DistributedLockUtil.class);
//	public static final String module = DistributedLockUtil.class.getName();
//	public static final String sytemId = System.getProperty("bosentNodeId");
//	@Value("${distributed.lock.timeout}")
//	public static long unlocktime=60000L;
//	@Value("${distributed.lock.type}")
//	public static String type ;
//	public static int waittime = 200;
//	public static final String entityname = "DistributedLock";
////	public static CacheClient redisClient;
//	public static String DBTYPE = "DB";
//	public static String REDISTYPE = "REDIS";
//	private static DistributedLockUtil distributedLock = null;
//
//	protected DistributedLockUtil() {
//	}
//
//	public static synchronized DistributedLockUtil getFactoryInstance() {
//		System.out.println(type);
//		if (distributedLock == null) {
//			if (DBTYPE.equals(type)) {
////				init();
//				distributedLock = new DistributedLockDb();
//			}
////			else if (REDISTYPE.equals(type)) {
////				redisClient = CacheClient.getCacheClient();
////				distributedLock = new DistributedLockRedis();
////			}
//
//			if (distributedLock == null) {
//				logger.warn("***如果实在没有实例化的分布式数据锁，将使用数据库分布式锁***", type);
//				distributedLock = new DistributedLockDb();
//			}
//		}
//
//		return distributedLock;
//	}
//
//	/**
//	 * 获取共享锁，未获取锁的节点等待持有锁
//	 * @param lockName
//	 */
//	public void getLock(String lockName) {
//	}
//
//	public void getLock(String lockName, long unlocktime) {
//	}
//
//	public void unLock(String lockName) {
//	}
//
//	/**
//	 * 查看锁是否存在
//	 * @param lockName
//	 * @return
//	 */
//	public boolean isLock(String lockName) {return false;}
//
//	/**
//	 * 获取排他锁，未获取锁就直接退出
//	 * @param lockName
//	 * @return
//	 */
//	public boolean getExclusiveLock(String lockName) {
//		return getExclusiveLock(lockName, 60000l);
//	}
//
//	/**
//	 * 获取排他锁，未获取锁就直接退出
//	 * @param lockName
//	 * @param unlocktime
//	 * @return
//	 */
//	public boolean getExclusiveLock(String lockName, long unlocktime) {
//		return false;
//	}
//}
