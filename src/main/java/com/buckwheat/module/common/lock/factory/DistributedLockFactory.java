package com.buckwheat.module.common.lock.factory;

import com.buckwheat.common.util.UtilValidate;
import com.buckwheat.module.common.lock.DistributedLockDb;
import com.buckwheat.module.common.lock.DistributedLockRedis;
import com.buckwheat.module.common.lock.impl.IDistributedLock;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
/**
 * @author liuc
 */
@Component
@Log4j2
public class DistributedLockFactory {
    private static IDistributedLock distributedLock = null;
    public static String DBTYPE = "DB";
    public static String REDISTYPE = "REDIS";


    public static synchronized IDistributedLock getDistributedLock(String type){
        if (UtilValidate.areEqual(type,DBTYPE)) {
            distributedLock = new DistributedLockDb();
        }
        if (UtilValidate.areEqual(type,REDISTYPE)) {
            distributedLock = new DistributedLockRedis();
        }
        if (distributedLock == null) {
            log.warn("***如果实在没有实例化的分布式数据锁，将使用数据库分布式锁***", type);
            distributedLock = new DistributedLockDb();
        }
        return distributedLock;
    }
}
