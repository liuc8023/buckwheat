package com.buckwheat.module.common.lock;

import com.buckwheat.common.util.RedisUtils;
import com.buckwheat.common.util.SpringContextUtil;
import com.buckwheat.common.util.UtilValidate;
import com.buckwheat.module.common.lock.config.DistributedLockConfig;
import com.buckwheat.module.common.lock.impl.IDistributedLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Redis分布式锁
 * @author liuc
 */
public class DistributedLockRedis implements IDistributedLock {
	private static final Logger logger = LoggerFactory.getLogger(DistributedLockRedis.class);
	public static final String MODULE = DistributedLockDb.class.getName();
	public DistributedLockConfig distributedLockConfig = (DistributedLockConfig) SpringContextUtil.getBean("distributedLockConfig");
	public long unlocktime = distributedLockConfig.getUnlocktime();
	@Override
	public void getLock(String lockName) {
		boolean is = RedisUtils.tryLock(lockName,unlocktime);
		if(is) {
			logger.info("****使用Redis分布式锁，上锁成功，lock="+lockName+"****", MODULE);
		}else {
			logger.warn("****使用Redis分布式锁，上锁失败，lock="+lockName+"****", MODULE);
		}
	}

    @Override
    public void getLock(String lockName, long unlocktime) {

    }

    @Override
	public void unLock(String lockName) {
		boolean is = RedisUtils.unlock(lockName);
		if(is) {
			logger.info("****使用Redis分布式锁，解锁成功，lock="+lockName+"****", MODULE);
		}else {
			logger.warn("****使用Redis分布式锁，解锁失败，lock="+lockName+"****", MODULE);
		}
	}

    @Override
    public boolean isLock(String lockName) {
		if (UtilValidate.isNotEmpty(RedisUtils.get(lockName))) {
			return true;
		}
        return false;
    }

    @Override
	public boolean getExclusiveLock(String lockName) {
		boolean is =getExclusiveLock(lockName,unlocktime);
		if(is) {
			logger.info("****使用Redis分布式锁，上锁成功，lock="+lockName+"****", MODULE);
		}else {
			logger.warn("****使用Redis分布式锁，上锁失败，lock="+lockName+"****", MODULE);
		}
		return is;
	}

    @Override
    public boolean getExclusiveLock(String lockName, long unlocktime) {
		boolean is = RedisUtils.tryLock(lockName,unlocktime);
		if(is) {
			logger.info("****使用Redis分布式锁，上锁成功，lock="+lockName+"****", MODULE);
		}else {
			logger.warn("****使用Redis分布式锁，上锁失败，lock="+lockName+"****", MODULE);
		}
		return is;
    }

}
