package com.buckwheat.module.common.lock.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuc
 * 分布式事务
 */
@Configuration
@Log4j2
public class DistributedLockConfig {

    public long unlocktime;

    public String type ;

    public int waittime;

    public long getUnlocktime() {
        return unlocktime;
    }
    @Value("${distributed.lock.timeout}")
    public void setUnlocktime(long unlocktime) {
        this.unlocktime = unlocktime;
    }

    public String getType() {
        return type;
    }
    @Value("${distributed.lock.type}")
    public void setType(String type) {
        this.type = type;
    }

    public int getWaittime() {
        return waittime;
    }
    @Value("${distributed.lock.waittime}")
    public void setWaittime(int waittime) {
        this.waittime = waittime;
    }
}
