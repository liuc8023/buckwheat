package com.buckwheat.module.common.lock;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.buckwheat.common.util.DateUtil;
import com.buckwheat.common.util.SpringContextUtil;
import com.buckwheat.common.util.UtilValidate;
import com.buckwheat.module.common.entity.DistributedLock;
import com.buckwheat.module.common.lock.config.DistributedLockConfig;
import com.buckwheat.module.common.lock.impl.IDistributedLock;
import com.buckwheat.module.common.service.DistributedLockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * 分布式数据库共享锁
 * @author liuc
 */
@Configuration
public class DistributedLockDb implements IDistributedLock {
	public DistributedLockService distributedLockService = (DistributedLockService) SpringContextUtil.getBean("distributedLockServiceImpl");
	public DistributedLockConfig distributedLockConfig = (DistributedLockConfig) SpringContextUtil.getBean("distributedLockConfig");
	private static final Logger logger = LoggerFactory.getLogger(DistributedLockDb.class);
	public static final String MODULE = DistributedLockDb.class.getName();
	public static final String sytemId = System.getProperty("bosentNodeId");
	public long unlocktime = distributedLockConfig.getUnlocktime();
	public int waittime = distributedLockConfig.getWaittime();

	@Override
	public void getLock(String lockName) {
		getLock(lockName,unlocktime);
	}

	/**
	 * 						select  ←-------------------------
	 * 			______________|_________________               |
	 *            ↓                                                             ↓                            |
	 *  		  none						      has      		   |
	 *         (create)                     ______|_______        |
	 *  	                                |		            ↓               |
	 *                                  timeout        notimeout →|
	 * 							 (delete&&create)
	 **/
	@Override
	public void getLock(String lockName, long unlocktime) {
		logger.info("***调用分布式数据库共享锁，锁名称为"+lockName+"***", MODULE);
		Map<String, String> result = new HashMap<>(8);
		try {
			Map<String, Object> columnMap = new HashMap<>(8);
			columnMap.put("lock_name",lockName);
			List<DistributedLock> list = new ArrayList<>();
			try {
				list = distributedLockService.listByMap(columnMap);
			} catch (Exception e) {
				logger.error("***查询数据库分布式锁["+lockName+"]信息失败,原因 "+e.getMessage(), MODULE);
			}
			if (UtilValidate.isNotEmpty(list)) {
				result.put("endTime",list.get(0).getEndTime());
				waitLock(lockName, result,unlocktime);
			} else {
				createLock(lockName,unlocktime);
			}

		} catch (Exception e) {
			logger.error("***获取分布式数据库共享锁["+lockName+"]失败,原因："+e.getMessage(), MODULE);
		}

	}


	private void createLock(String lockName,long unlocktime){
		boolean execResult = false;
		try {
			DistributedLock distributedLock = new DistributedLock();
			distributedLock.setLockName(lockName);
			distributedLock.setHolder(sytemId);
			long time = System.currentTimeMillis()+unlocktime;
			distributedLock.setEndTime(DateUtil.covertObjToString(time));
			distributedLock.setCreatedStamp(DateUtil.convertObjToTimestamp(DateUtil.getCurrDateTime()));
			distributedLock.setCreatedTxStamp(DateUtil.convertObjToTimestamp(DateUtil.getCurrDateTime()));
			distributedLock.setLastUpdatedStamp(DateUtil.convertObjToTimestamp(DateUtil.getCurrDateTime()));
			distributedLock.setLastUpdatedTxStamp(DateUtil.convertObjToTimestamp(DateUtil.getCurrDateTime()));
			try {
				execResult = distributedLockService.saveOrUpdate(distributedLock);
			} catch (Exception e) {
				logger.error("***创建数据库分布式锁[" + lockName + "]信息失败,原因 " + e.getMessage(), MODULE);
			}
			if(!execResult) {
				getLock(lockName);
			}else {
				logger.info("成功持有分布式共享锁"+lockName+",超时间"+unlocktime+"毫秒", MODULE);
			}
		} catch (Exception e) {
			getLock(lockName);
		}
	}

	private boolean createOnlyLock(String lockName,long unlocktime){
		boolean execResult = true;
		try {
			DistributedLock distributedLock = new DistributedLock();
			distributedLock.setLockName(lockName);
			distributedLock.setHolder(sytemId);
			long time = System.currentTimeMillis()+unlocktime;
			distributedLock.setEndTime(DateUtil.covertObjToString(time));
			distributedLock.setCreatedStamp(DateUtil.convertObjToTimestamp(DateUtil.getCurrDateTime()));
			distributedLock.setCreatedTxStamp(DateUtil.convertObjToTimestamp(DateUtil.getCurrDateTime()));
			distributedLock.setLastUpdatedStamp(DateUtil.convertObjToTimestamp(DateUtil.getCurrDateTime()));
			distributedLock.setLastUpdatedTxStamp(DateUtil.convertObjToTimestamp(DateUtil.getCurrDateTime()));
			try {
				execResult = distributedLockService.saveOrUpdate(distributedLock);
			} catch (Exception e) {
				logger.error("***创建数据库分布式锁["+lockName+"]信息失败,原因 "+e.getMessage(), MODULE);
			}
			logger.info("成功持有分布式互斥锁"+lockName+",超时间"+unlocktime+"毫秒", MODULE);
		} catch (Exception e) {
			logger.error("***创建数据库分布式锁["+lockName+"]信息失败,原因 "+e.getMessage(), MODULE);
			throw new RuntimeException(e);
		}
		return execResult;
	}

	/**
	 * 删除数据库分布式锁
	 * @param lockName
	 * @param owner
	 */
	private void deleteLock(String lockName,boolean owner){
		Map<String, Object> columnMap = new HashMap<>(8);
		if(owner) {
			columnMap.put("lock_name",lockName);
			columnMap.put("holder",sytemId);
		}else {
			columnMap.put("lock_name",lockName);
		}
		try {
			distributedLockService.removeByMap(columnMap);
			logger.info("***删除数据库分布式锁"+lockName, MODULE);
		} catch (Exception e) {
			logger.error("***删除数据库分布式锁["+lockName+"]失败,原因："+e.getMessage(), MODULE);
		}
	}

	private void waitLock(String lockName, Map<String, String> param,long unlockTime) {
		long timeout = System.currentTimeMillis();
		long endTime = 0L;
		String endTimeStr = param.get("endTime");
		if(!UtilValidate.isEmpty(endTimeStr)) {
			endTime = Long.valueOf(endTimeStr);
		}

		while(timeout < endTime) {
			Map<String, Object> columnMap = new HashMap<>(8);
			columnMap.put("lock_name",lockName);
			List<DistributedLock> list = null;
			try {
				list = distributedLockService.listByMap(columnMap);
			} catch (Exception e) {
				logger.error("***查询数据库分布式锁["+lockName+"]信息失败,原因 "+e.getMessage(), MODULE);
			}

			if(UtilValidate.isEmpty(list)) {
				createLock(lockName,unlocktime);
				return;
			}
			logger.info("***分布式数据库锁已被持有，lockName="+lockName+", 存活时间"+(endTime-timeout)+"***", MODULE);
			try {
				Thread.sleep(waittime);
			} catch (InterruptedException e) {
				logger.error("***线程等待创建分布式锁["+lockName+"]失败 "+e.getMessage(), MODULE);
			}

			timeout += waittime;
		}

		try {
			deleteLock(lockName, false);
			createLock(lockName,unlocktime);
		} catch (Exception e) {
			logger.error("***DistributedLock超时删除旧锁["+lockName+"]，创建新锁["+lockName+"]失败,原因："+e.getMessage(), MODULE);
		}

	}

	/**
	 * 解除分布式数据库锁
	 * @param lockName
	 */
	@Override
	public void unLock(String lockName) {
		try {
			deleteLock(lockName, true);
			logger.info("***分布式数据库锁已被解除，lockName="+lockName+"***", MODULE);
		} catch (Exception e) {
			logger.error("***DistributedLock删除锁["+lockName+"]失败,原因："+e.getMessage(), MODULE);
		}

	}

	@Override
	public boolean isLock(String lockName) {
		try {
			Map<String, Object> columnMap = new HashMap<>(8);
			columnMap.put("lock_name",lockName);
			List<DistributedLock> list = null;
			try {
				list = distributedLockService.listByMap(columnMap);
			} catch (Exception e) {
				logger.error("***查询数据库分布式锁["+lockName+"]信息失败,原因 "+e.getMessage(), MODULE);
			}
			if(UtilValidate.isNotEmpty(list)) {
				return true;
			}else {
				return false;
			}

		} catch (Exception e) {
			logger.error("查询数据库分布式锁["+lockName+"]失败,原因："+e.getMessage(), MODULE);
		}
		return false;
	}

	@Override
	public boolean getExclusiveLock(String lockName) {
		return getExclusiveLock(lockName, unlocktime);
	}

	@Override
	public boolean getExclusiveLock(String lockName, long unlockTime) {
		boolean execResult=true;
		logger.info("***调用分布式数据库排他锁，锁名称为"+lockName, MODULE);
		Map<String, String> result = new HashMap<>(8);
		List<DistributedLock> list = null;
		try {
			Map<String, Object> columnMap = new HashMap<>(8);
			columnMap.put("lock_name",lockName);
			list = distributedLockService.listByMap(columnMap);
			if(UtilValidate.isNotEmpty(list)) {
				LocalDateTime currentTime = DateUtil.getCurrLocalDateTime();
				LocalDateTime endTime = null;
				String endTimeStr = list.get(0).getEndTime();
				if(!UtilValidate.isEmpty(endTimeStr)) {
					endTime = DateUtil.convertObjToLdt(endTimeStr).plusSeconds(unlockTime/6000);

				}
				if(currentTime.isAfter(endTime)) {
					deleteLock(lockName, false);
					logger.warn("***分布式锁["+lockName+"]已经失效超时并删除", MODULE);
				}else {
					logger.warn("***分布式锁["+lockName+"]已经被有效持有", MODULE);
					return false;
				}
			}
			execResult = createOnlyLock(lockName, unlockTime);
		} catch (Exception e) {
			logger.error("***获取分布式数据库排他锁["+lockName+"]失败,原因："+e.getMessage(), MODULE);
			return false;
		}
		return execResult;
	}

}
