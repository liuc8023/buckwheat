package com.buckwheat.module.common.lock.impl;

/**
 * @author liuc
 */
public interface IDistributedLock {
    public void getLock(String lockName);
    public void getLock(String lockName, long unlocktime);
    public void unLock(String lockName);
    public boolean isLock(String lockName);
    public boolean getExclusiveLock(String lockName);
    public boolean getExclusiveLock(String lockName, long unlocktime);
}
