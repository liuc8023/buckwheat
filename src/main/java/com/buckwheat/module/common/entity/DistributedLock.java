package com.buckwheat.module.common.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.buckwheat.common.annotation.InitDataSource;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;

/**
 * @className: DistributedLock
 * @description: 分布式锁表
 * @author: liuc
 * @date: 2019-11-12
 */
@Data
@Accessors(chain = true)
@TableName("distributed_lock")
@ApiModel(value="DistributedLock对象",description="分布式锁表")
@InitDataSource({"distributed_lock"})
public class DistributedLock implements Serializable{

    private static final long serialVersionUID=1L;

    /**
     * 分布式锁名称(唯一主键)
     */
    @TableId(value = "lock_name", type = IdType.INPUT)
    @ApiModelProperty(value="分布式锁名称(唯一主键)",name="lockName",example="")
    private String lockName;

    /**
     * 持有节点(每个节点自动生成的uuid
     */
    @TableField(value = "holder")
    @ApiModelProperty(value="持有节点(每个节点自动生成的uuid)",name="holder",example="")
    private String holder;

    /**
     * 锁的结束时间
     */
    @TableField(value = "end_time")
    @ApiModelProperty(value="锁的结束时间",name="endTime",example="")
    private String endTime;

    /**
     * 创建时间
     */
    @TableField(value = "created_stamp",fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建时间",name="createdStamp",example="")
    private Date createdStamp;

    /**
     * 创建事务时间
     */
    @TableField(value = "created_tx_stamp",fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建事务时间",name="createdTxStamp",example="")
    private Date createdTxStamp;

    /**
     * 最后更新时间
     */
    @TableField(value = "last_updated_stamp",fill = FieldFill.UPDATE)
    @ApiModelProperty(value="最后更新时间",name="lastUpdatedStamp",example="")
    private Date lastUpdatedStamp;

    /**
     * 最后更新事务时间
     */
    @TableField(value = "last_updated_tx_stamp",fill = FieldFill.UPDATE)
    @ApiModelProperty(value="最后更新事务时间",name="lastUpdatedTxStamp",example="")
    private Date lastUpdatedTxStamp;

}
