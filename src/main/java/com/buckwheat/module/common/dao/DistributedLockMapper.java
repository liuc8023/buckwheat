package com.buckwheat.module.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.buckwheat.module.common.entity.DistributedLock;
import org.apache.ibatis.annotations.Mapper;

/**
 * 分布式锁表 Mapper 接口
 * @className: DistributedLockMapper
 * @author: liuc
 * @date: 2019-11-12
 */
@Mapper
public interface DistributedLockMapper extends BaseMapper<DistributedLock> {

}
