package com.buckwheat.module.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.buckwheat.module.common.entity.DistributedLock;

/**
 * 分布式锁表服务类
 * @className: DistributedLockService
 * @author: liuc
 * @date: 2019-11-12
 */
public interface DistributedLockService extends IService<DistributedLock> {

}