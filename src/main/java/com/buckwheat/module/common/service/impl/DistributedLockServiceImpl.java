package com.buckwheat.module.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.buckwheat.module.common.dao.DistributedLockMapper;
import com.buckwheat.module.common.entity.DistributedLock;
import com.buckwheat.module.common.service.DistributedLockService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 分布式锁表服务实现类
 * @className: DistributedLockServiceImpl
 * @author: liuc
 * @date: 2019-11-12
 */
@Service
public class DistributedLockServiceImpl extends ServiceImpl<DistributedLockMapper, DistributedLock> implements DistributedLockService {
    @Resource
    private DistributedLockMapper mapper;
    @Override
    public List<DistributedLock> listByMap(Map<String, Object> columnMap) {
        return super.listByMap(columnMap);
    }

    @Override
    public boolean removeByMap(Map<String, Object> columnMap) {
        return super.removeByMap(columnMap);
    }
}