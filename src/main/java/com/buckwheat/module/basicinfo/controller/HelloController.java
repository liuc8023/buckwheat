package com.buckwheat.module.basicinfo.controller;

import com.alibaba.fastjson.JSONObject;
import com.buckwheat.common.exception.BusinessException;
import com.buckwheat.common.init.UtilCommonCode;
import com.buckwheat.common.message.RequestInfo;
import com.buckwheat.common.message.ResponseBuilder;
import com.buckwheat.common.message.ResponseInfo;
import com.buckwheat.common.util.DateUtil;
import com.buckwheat.common.util.JsonUtil;
import com.buckwheat.common.util.RedisUtil;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/11/25 16:07
 * @since JDK1.8
 */
@RestController
@Log4j2
public class HelloController {
    @Resource
    private ResponseBuilder responseBuilder;

    @SneakyThrows
    @PostMapping("/hello")
    @ApiOperation(value = "hello",httpMethod = "POST",notes = "hello",consumes = "application/json",produces = "application/json")
    public ResponseInfo hello(@RequestBody RequestInfo requestInfo) {
        ResponseInfo responseInfo = responseBuilder.buildResponse(requestInfo);
        log.info("码值CD003602是："+ UtilCommonCode.getCodeValueByType("CD003600", "CD003602"));
//        try {
//            Map<String,Object> map =null;
//                    DateUtil.convertObjToLd("1111111111111111111111");
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new BusinessException(e.getMessage());
//        }
        log.error("ERROR 级别日志");
        log.warn("WARN 级别日志");
        log.info("INFO 级别日志");
        log.debug("DEBUG 级别日志");
        log.trace("TRACE 级别日志");
//        RedisUtil.set("test", 1234);
//        log.info("test:{}",RedisUtil.get("test"));
//        RedisUtil.set("name","liuc",10);
        Map requestInfoMap = null;
        try {
            requestInfoMap = JsonUtil.jsonToMap(JsonUtil.toJson(requestInfo));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String,Object> context = (Map<String, Object>) requestInfoMap.get("requestBody");
        Map<String,Object> map = new HashMap<>(8);
        map.put("name",String.format("Hello %s!", (String)context.get("name")));
        responseInfo.setResponseBody(map);
        return responseInfo;
    }



    @PostMapping("/test")
    @ApiOperation(value = "test",httpMethod = "POST",notes = "hello",consumes = "application/json",produces = "application/json")
    public Map<String, Object> test(@RequestBody RequestInfo requestInfo) {
        ResponseInfo responseInfo = responseBuilder.buildResponse(requestInfo);
        log.info("码值CD003602是："+ UtilCommonCode.getCodeValueByType("CD003600", "CD003602"));
        log.error("ERROR 级别日志");
        log.warn("WARN 级别日志");
        log.info("INFO 级别日志");
        log.debug("DEBUG 级别日志");
        log.trace("TRACE 级别日志");
//        RedisUtil.set("test", 1234);
//        log.info("test:{}",RedisUtil.get("test"));
//        RedisUtil.set("name","liuc",10);
        Map requestInfoMap = null;
        try {
            requestInfoMap = JsonUtil.jsonToMap(JsonUtil.toJson(requestInfo));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String,Object> context = (Map<String, Object>) requestInfoMap.get("requestBody");
        Map<String,Object> map = new HashMap<>(8);
        map.put("name",String.format("Hello %s!", (String)context.get("name")));
        return map;
    }

}
