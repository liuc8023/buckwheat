package com.buckwheat.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/11/27 15:24
 * @since JDK1.8
 */
@Configuration
@Log4j2
public class BuckwheatServletInitializer implements CommandLineRunner {

    @Override
    public void run(String... args){
        log.info("=====Buckwheat应用已经成功启动=====");
    }
}
