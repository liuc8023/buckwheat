package com.buckwheat.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/11/27 16:59
 * @since JDK1.8
 */
@Configuration
public class ShutDownConfig {

    /**
     * 用于接受 shutdown 事件
     * @param
     * @return com.buckwheat.config.TerminateBean
     * @author liuc
     * @date 2021/11/27 17:06
     * @throws
     */
    @Bean
    public BuckwheatShutdown buckwheatShutdown() {
        return new BuckwheatShutdown();
    }

}
