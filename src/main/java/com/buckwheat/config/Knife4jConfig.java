package com.buckwheat.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/11/21 12:07
 * @since JDK1.8
 */
@Configuration
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
@ConditionalOnProperty(value = {"knife4j.enable"}, matchIfMissing = true)
public class Knife4jConfig {
    @Bean
    public Docket api() {
        // 选择swagger2版本
        return new Docket(DocumentationType.SWAGGER_2)
                //定义api文档汇总信息
                .apiInfo(apiInfo())
                .groupName("buckwheat")
                .select()
                .apis(RequestHandlerSelectors
                // 指定生成api文档的包
                .basePackage("com.buckwheat"))
                // 指定所有路径
                .paths(PathSelectors.any())
                .build()
                ;
    }

    /**
     * 构建文档api信息
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 文档标题
                .title("buckwheat RESTful APIs")
                //联系人信息
                .contact(new Contact("liuc", "url", "mail"))
                //描述
                .description("描述")
                //文档版本号
                .version("0.1")
                //网站地址
                .termsOfServiceUrl("http://localhost:8090")
                .build();
    }
}
