package com.buckwheat.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.buckwheat.common.annotation.InitDataSource;
import com.buckwheat.common.exception.BusinessErrorCode;
import com.buckwheat.common.exception.BusinessException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import javax.annotation.Resource;

/**
 * 初始化数据库配置类
 * @author liuc
 * @version V1.0
 * @date 2021/11/15 15:26
 * @since JDK1.8
 */
@Configuration
@Log4j2
public class InitDataSourceConfig {
    private static final String MODULE = InitDataSourceConfig.class.getName();
    @Resource
    DruidDataSource dataSource;

    /**
     * 初始化数据库表及表数据
     * @param
     * @return org.springframework.jdbc.datasource.init.DataSourceInitializer
     * @author liuc
     * @date 2021/11/15 21:10
     * @throws
     */
    @Bean("dataSourceInitializer")
    public DataSourceInitializer dataSourceInitializer() {
        log.info("初始化数据库");
        //获取开始时间
        long startTime = System.currentTimeMillis();
        final DataSourceInitializer initializer = new DataSourceInitializer();
        // 设置数据源
        initializer.setDataSource(dataSource);
        initializer.setDatabasePopulator(databasePopulator());
        log.info("初始化数据库完成");
        //获取结束时间
        long endTime = System.currentTimeMillis();
        log.info("初始化数据库完成："+(endTime-startTime)+"毫秒！",MODULE);
        return initializer;
    }

    private DatabasePopulator databasePopulator() {
        final ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        // 扫描com.buckwheat 包 找到InitDataSource注解的类(注解需使用到实现类上)
        ClassPathScanningCandidateComponentProvider provider
                = new ClassPathScanningCandidateComponentProvider(false);
        //添加包含的过滤信息
        provider.addIncludeFilter(new AnnotationTypeFilter(InitDataSource.class));
        for (BeanDefinition beanDef : provider.findCandidateComponents("com.buckwheat")) {
            Class<?> cl = null;
            try {
                cl = Class.forName(beanDef.getBeanClassName());
                InitDataSource initDataSource = cl.getAnnotation(InitDataSource.class);
                String[] sqlFiles = initDataSource.value();
                for (String sql: sqlFiles) {
                    // 如果sql文件存在 加入数据库初始化中 否则抛出异常终止执行
                    ClassPathResource resource = new ClassPathResource("/datasource/" + sql+".sql");
                    if (resource.exists()) {
                        populator.addScript(resource);
                    } else {
                        throw new BusinessException(BusinessErrorCode.CSRCB30009, sql);
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return populator;
    }
}
