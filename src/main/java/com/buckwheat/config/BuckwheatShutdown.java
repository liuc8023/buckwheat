package com.buckwheat.config;

import lombok.extern.log4j.Log4j2;
import javax.annotation.PreDestroy;

/**
 * 优雅关闭 Spring Boot。容器必须是 tomcat
 * @author liuc
 * @version V1.0
 * @date 2021/11/27 17:11
 * @since JDK1.8
 */
@Log4j2
public class BuckwheatShutdown {
    @PreDestroy
    public void preDestroy() {
        log.info("BuckwheatShutdown is destroyed");
    }
}
