package com.buckwheat;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * {@link ApplicationRunner} 实现类
 * @author liuc
 * @version V1.0
 * @date 2021/11/12 17:28
 * @since JDK1.8
 */
@Component
public class ApplicationRunnerSupport implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("runner:");
        boolean isHaveSkip = args.containsOption("skip");
        System.out.println("skip：" + isHaveSkip);
        System.out.println(args.getOptionValues("skip"));
    }

}
