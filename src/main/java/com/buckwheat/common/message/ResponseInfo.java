package com.buckwheat.common.message;

import com.buckwheat.common.exception.BusinessErrorCode;
import com.buckwheat.common.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/3 14:06
 * @since JDK1.8
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value="响应报文信息",description="响应报文信息")
public class ResponseInfo<T>{
    private ResponseHead responseHead;

    private T responseBody;

    @JsonIgnore
    public void setSuccess()
    {
        ResponseHead responseHead = getResponseHead();
        if (responseHead == null) {
            responseHead = new ResponseHead();
            setResponseHead(responseHead);
        }

        responseHead.setReturnStatus("S");
        ResponseCode code = new ResponseCode();
        code.setReturnCode("000000");
        code.setReturnMsg("交易成功");
        responseHead.setRet(new ResponseCode[] { code });
    }
    @JsonIgnore
    public void setError(BusinessErrorCode error) {
        setError(error.getCode(), error.getMsg());
    }
    @JsonIgnore
    public void setError(BusinessErrorCode error, String message) {
        setError(error.getCode(), message);
    }

    @JsonIgnore
    public void setError(BusinessException exption)
    {
        setError(exption.getErrorCode().getCode(), exption.getMessage());
    }

    @JsonIgnore
    public void setError(String errCode, String mesage)
    {
        ResponseHead responseHead = getResponseHead();
        if (responseHead == null) {
            responseHead = new ResponseHead();
            setResponseHead(responseHead);
        }

        responseHead.setReturnStatus("F");
        ResponseCode code = new ResponseCode();
        code.setReturnCode(errCode);
        code.setReturnMsg(mesage);
        responseHead.setRet(new ResponseCode[] { code });
    }
    @JsonIgnore
    public boolean isSuccess() {
        return "S".equalsIgnoreCase(getResponseHead().getReturnStatus());
    }

    public ResponseHead getResponseHead() {
        return responseHead;
    }

    public void setResponseHead(ResponseHead responseHead) {
        this.responseHead = responseHead;
    }

    public T getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(T responseBody) {
        this.responseBody = responseBody;
    }

    @Override
    public String toString() {
        return "ResponseInfo{" +
                "responseHead=" + responseHead +
                ", responseBody=" + responseBody +
                '}';
    }
}
