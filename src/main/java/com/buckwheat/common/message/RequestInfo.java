package com.buckwheat.common.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/3 10:36
 * @since JDK1.8
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value="请求报文信息",description="请求报文信息")
public class RequestInfo<T> {
    private RequestHead requestHead;
    private T requestBody;

    public RequestHead getRequestHead() {
        return requestHead;
    }

    public void setRequestHead(RequestHead requestHead) {
        this.requestHead = requestHead;
    }

    public T getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(T requestBody) {
        this.requestBody = requestBody;
    }

    @Override
    public String toString() {
        return "HubRequestInfo{" +
                "requestHead=" + requestHead +
                ", requestBody=" + requestBody +
                '}';
    }
}
