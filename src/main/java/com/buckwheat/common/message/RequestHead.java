package com.buckwheat.common.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/3 10:20
 * @since JDK1.8
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value="请求报文头",description="请求报文头")
public class RequestHead {
    /**
     * 服务代码
     */
    @ApiModelProperty(value="服务代码",name="serviceCode",required = true,example="")
    private String serviceCode;

    /**
     * 服务场景
     */
    @ApiModelProperty(value="服务场景",name="serviceScene",required = true,example="")
    private String serviceScene;

    /**
     * 消费系统编号
     */
    @ApiModelProperty(value="消费系统编号",name="consumerId",required = true,example="")
    private String consumerId;

    /**
     * 请求方渠道类型
     */
    @ApiModelProperty(value="请求方渠道类型",name="channelTyp",required = false,example="")
    private String channelTyp;

    /**
     * 发起方系统编号
     */
    @ApiModelProperty(value="发起方系统编号",name="orgConsumerId",required = true,example="")
    private String orgConsumerId;

    /**
     * 系统流水号
     */
    @ApiModelProperty(value="系统流水号",name="consumerSeqNo",required = true,example="")
    private String consumerSeqNo;

    /**
     * 发起方业务流水号
     */
    @ApiModelProperty(value="发起方业务流水号",name="orgConsumerSeqNo",required = true,example="")
    private String orgConsumerSeqNo;

    /**
     * 交易日期
     */
    @ApiModelProperty(value="交易日期 yyyyMMdd格式",name="tranDate",required = true,example="20191101")
    private String tranDate;

    /**
     * 交易时间
     */
    @ApiModelProperty(value="交易时间 HHmmss格式",name="tranTime",required = true,example="104327")
    private String tranTime;

    /**
     * 终端号
     */
    @ApiModelProperty(value="终端号",name="terminalCode",required = false,example="")
    private String terminalCode;

    /**
     * 发起方终端号
     */
    @ApiModelProperty(value="发起方终端号",name="orgTerminalCode",required = false,example="")
    private String orgTerminalCode;

    /**
     * 消费系统服务器标识(IP地址)
     */
    @ApiModelProperty(value="消费系统服务器标识(IP地址)",name="consumerSvrId",required = false,example="127.0.0.1")
    private String consumerSvrId;

    /**
     * 发起方系统服务器标识(IP地址)
     */
    @ApiModelProperty(value="发起方系统服务器标识(IP地址)",name="orgConsumerSvrId",required = false,example="127.0.0.1")
    private String orgConsumerSvrId;

    /**
     * MAC值
     */
    private String mac;

    /**
     * 密匙Id(用于Pin,Mac)
     */
    @ApiModelProperty(value="密匙Id",name="keyId",required = false,example="")
    private String keyId;

    /**
     * 服务名
     */
    @ApiModelProperty(value="服务名",name="serviceName",required = false,example="")
    private String serviceName;

    /**
     * 交易码
     */
    @ApiModelProperty(value="交易码",name="tranCode",required = true,example="")
    private String tranCode;

    /**
     * 每页记录数
     */
    @ApiModelProperty(value="每页记录数",name="pageSize",required = false,example="")
    private Integer pageSize;

    /**
     * 当前页码
     */
    @ApiModelProperty(value="当前页码",name="pageNo",required = false,example="")
    private Integer pageNo;

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceScene() {
        return serviceScene;
    }

    public void setServiceScene(String serviceScene) {
        this.serviceScene = serviceScene;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getChannelTyp() {
        return channelTyp;
    }

    public void setChannelTyp(String channelTyp) {
        this.channelTyp = channelTyp;
    }

    public String getOrgConsumerId() {
        return orgConsumerId;
    }

    public void setOrgConsumerId(String orgConsumerId) {
        this.orgConsumerId = orgConsumerId;
    }

    public String getConsumerSeqNo() {
        return consumerSeqNo;
    }

    public void setConsumerSeqNo(String consumerSeqNo) {
        this.consumerSeqNo = consumerSeqNo;
    }

    public String getOrgConsumerSeqNo() {
        return orgConsumerSeqNo;
    }

    public void setOrgConsumerSeqNo(String orgConsumerSeqNo) {
        this.orgConsumerSeqNo = orgConsumerSeqNo;
    }

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public String getTranTime() {
        return tranTime;
    }

    public void setTranTime(String tranTime) {
        this.tranTime = tranTime;
    }

    public String getTerminalCode() {
        return terminalCode;
    }

    public void setTerminalCode(String terminalCode) {
        this.terminalCode = terminalCode;
    }

    public String getOrgTerminalCode() {
        return orgTerminalCode;
    }

    public void setOrgTerminalCode(String orgTerminalCode) {
        this.orgTerminalCode = orgTerminalCode;
    }

    public String getConsumerSvrId() {
        return consumerSvrId;
    }

    public void setConsumerSvrId(String consumerSvrId) {
        this.consumerSvrId = consumerSvrId;
    }

    public String getOrgConsumerSvrId() {
        return orgConsumerSvrId;
    }

    public void setOrgConsumerSvrId(String orgConsumerSvrId) {
        this.orgConsumerSvrId = orgConsumerSvrId;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getTranCode() {
        return tranCode;
    }

    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }
}
