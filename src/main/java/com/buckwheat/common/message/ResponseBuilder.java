package com.buckwheat.common.message;

import org.springframework.stereotype.Component;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/4 10:32
 * @since JDK1.8
 */
@Component
public class ResponseBuilder {
    public <T> ResponseInfo<T> buildResponse() {
        return buildResponse(new RequestHead(), null);
    }

    public <T> ResponseInfo<T> buildResponse(String serno)
    {
        return buildResponse(new RequestHead(), serno);
    }

    public <T> ResponseInfo<T> buildResponse(RequestInfo requestInfo) {
        return buildResponse(requestInfo.getRequestHead(),null);
    }

    public <T> ResponseInfo<T> buildResponse(RequestInfo requestInfo, String serno) {
        return buildResponse(requestInfo.getRequestHead(), serno);
    }

    @SuppressWarnings("unchecked")
    public <T> ResponseInfo<T> buildResponse(RequestHead requestHead, String serno) {
        ResponseInfo responseInfo = new ResponseInfo();
        ResponseHead responseHead = new ResponseHead();
        responseInfo.setResponseHead(responseHead);

        if (requestHead != null) {
            responseHead.setServiceCode(requestHead.getServiceCode());
            responseHead.setServiceScene(requestHead.getServiceScene());
            responseHead.setConsumerId(requestHead.getConsumerId());
            responseHead.setOrgConsumerId(requestHead.getOrgConsumerId());
            responseHead.setConsumerSeqNo(requestHead.getConsumerSeqNo());
            responseHead.setOrgConsumerSeqNo(requestHead.getOrgConsumerSeqNo());
            responseHead.setTerminalCode(requestHead.getTerminalCode());
            responseHead.setOrgTerminalCode(requestHead.getOrgTerminalCode());
            responseHead.setConsumerSvrId(requestHead.getConsumerSvrId());
            responseHead.setOrgConsumerSvrId(requestHead.getOrgConsumerSvrId());
            responseHead.setTranDate(requestHead.getTranDate());
            responseHead.setTranTime(requestHead.getTranTime());
        }
        return responseInfo;
    }
}
