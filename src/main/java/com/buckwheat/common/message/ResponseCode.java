package com.buckwheat.common.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 返回状态码
 * @author liuc
 * @version V1.0
 * @date 2021/12/3 15:11
 * @since JDK1.8
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value="响应码及响应信息",description="响应码及响应信息")
public class ResponseCode {
    /**
     * 交易返回码
     */
    @ApiModelProperty(value="交易返回码",name="returnCode",required = true,example="")
    private String returnCode;

    /**
     * 交易返回信息
     */
    @ApiModelProperty(value="交易返回信息",name="returnMsg",required = true,example="")
    private String returnMsg;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

}