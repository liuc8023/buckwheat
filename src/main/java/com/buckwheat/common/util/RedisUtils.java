package com.buckwheat.common.util;

import lombok.extern.log4j.Log4j2;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisCluster;
import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
@Log4j2
public class RedisUtils {
    @Resource
    private static JedisCluster jedisCluster;
    @Resource
    private static RedissonClient redissonClient;
    /**
     * 设置缓存
     * @param key    缓存key
     * @param value  缓存value
     */
    public static void set(String key, String value) {
        jedisCluster.set(key, value);
        log.debug("RedisUtil:set cache key={},value={}", key, value);
    }
    /**
     * 设置缓存对象
     * @param key    缓存key
     * @param obj  缓存value
     */
    public <T> void setObject(String key, T obj , int expireTime) {
        jedisCluster.setex(key, expireTime, JsonUtil.toJson(obj));
    }
    /**
     * 获取指定key的缓存
     * @param key---JSON.parseObject(value, User.class);
     */
    public static String getObject(String key) {
        return jedisCluster.get(key);
    }
    /**
     * 判断当前key值 是否存在
     *
     * @param key
     */
    public static boolean hasKey(String key) {
        return jedisCluster.exists(key);
    }
    /**
     * 设置缓存，并且自己指定过期时间
     * @param key
     * @param value
     * @param expireTime 过期时间
     */
    public static void setWithExpireTime( String key, String value, int expireTime) {
        jedisCluster.setex(key, expireTime, value);
        log.debug("RedisUtil:setWithExpireTime cache key={},value={},expireTime={}", key, value, expireTime);
    }
    /**
     * 获取指定key的缓存
     * @param key
     */
    public static String get(String key) {
        String value = jedisCluster.get(key);
        log.debug("RedisUtil:get cache key={},value={}",key, value);
        return value;
    }
    /**
     * 删除指定key的缓存
     * @param key
     */
    public static void delete(String key) {
        jedisCluster.del(key);
        log.debug("RedisUtil:delete cache key={}", key);
    }

    /**
     * 悲观锁机制-加锁
     */
    public static boolean tryLock(String lockName){
        return  tryLock(lockName);
    }

    /**
     * 悲观锁机制-加锁
     */
    public static boolean tryLock(String lockName,long unlocktime){
        //声明key对象
        String key = "LOCK_" + lockName;
        //获取锁对象
        RLock mylock = redissonClient.getLock(key);
        //加锁，并且设置锁过期时间3秒，防止死锁的产生  uuid+threadId
        try {
            mylock.tryLock(unlocktime, TimeUnit.MICROSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            //加锁成功
            return false;
        }
        //加锁成功
        return  true;
    }

    //锁的释放
    public static boolean unlock(String lockName) {
        //必须是和加锁时的同一个key
        String key = "LOCK_" + lockName;
        //获取所对象
        RLock mylock = redissonClient.getLock(key);
        //释放锁（解锁）
        mylock.unlock();
        //加锁成功
        return  true;
    }
}
