package com.buckwheat.common.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;

/**
 * JSON转换工具类
 * @author liuc
 * @version V1.0
 * @date 2021/11/1 14:56
 * @since JDK1.8
 */
@Log4j2
public class JsonUtil {
    private static ObjectMapper objectMapper = new ObjectMapper();

    static {
        // 对象字段全部列入
        objectMapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_DEFAULT);

        // 取消默认转换timestamps形式
        objectMapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);

        // 忽略空bean转json的错误
        objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);

        // 统一日期格式yyyy-MM-dd HH:mm:ss
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

        // 忽略在json字符串中存在,但是在java对象中不存在对应属性的情况
        objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * object转Json字符串
     * @param obj
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/2 14:53
     * @throws
     */
    public static <T> String objToString(T obj) {
        if (obj == null) {
            return null;
        }
        try {
            return obj instanceof String ? (String) obj : objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            log.error("Parse object to String error",e);
            return null;
        }
    }

    /**
     * Object转json字符串并格式化美化
     * @param obj
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/2 14:53
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static <T> String objToStringPretty(T obj) {
        if (obj == null) {
            return null;
        }
        try {
            return obj instanceof String ? (String) obj : objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (Exception e) {
            log.error("Parse object to String error",e);
            return null;
        }
    }

    /**
     * string转object
     * @param str json字符串
     * @param clazz 被转对象class
     * @return T
     * @author liuc
     * @date 2021/11/2 14:52
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static <T> T stringToObj(String str, Class<T> clazz) {
        if (StringUtils.isEmpty(str) || clazz == null) {
            return null;
        }
        try {
            return clazz.equals(String.class) ? (T) str : objectMapper.readValue(str, clazz);
        } catch (IOException e) {
            log.error("Parse String to Object error",e);
            return null;
        }
    }

    /**
     * string转object
     * @param str json字符串
     * @param typeReference 被转对象引用类型
     * @return T
     * @author liuc
     * @date 2021/11/2 14:52
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static <T> T stringToObjRef(String str, TypeReference<T> typeReference) {
        if (StringUtils.isEmpty(str) || typeReference == null) {
            return null;
        }
        try {
            return (T) (typeReference.getType().equals(String.class) ? str : objectMapper.readValue(str, typeReference));
        } catch (IOException e) {
            log.error("Parse String to Object error",e);
            return null;
        }
    }

    /**
     * string转collection 用于转为集合对象
     * @param str json字符串
     * @param collectionClass 被转集合class
     * @param elementClasses 被转集合中对象类型class
     * @return T
     * @author liuc
     * @date 2021/11/2 14:51
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static <T> T stringToCollection(String str, Class<?> collectionClass, Class<?>... elementClasses) {
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
        try {
            return objectMapper.readValue(str, javaType);
        } catch (IOException e) {
            log.error("Parse String to Collection error",e);
            return null;
        }
    }

    /**
     * 根据JSONArray String获取到List
     * @param class1
     * @param jArrayStr
     * @return java.util.List<T>
     * @author liuc
     * @date 2021/11/2 14:51
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getListByJSONArray(Class<T> class1, String jArrayStr) {
        List<T> list = new ArrayList<>();
        JSONArray jsonArray = JSONArray.parseArray(jArrayStr);
        if (jsonArray == null || jsonArray.isEmpty()) {
            return list;
        }
        for (Object object : jsonArray) {
            JSONObject jsonObject = (JSONObject) object;
            T t = JSONObject.toJavaObject(jsonObject, class1);
            list.add(t);
        }
        return list;
    }

    /**
     * 根据List获取到对应的JSONArray
     * @param list
     * @return com.alibaba.fastjson.JSONArray
     * @author liuc
     * @date 2021/11/2 14:51
     * @throws
     */
    public static JSONArray getJSONArrayByList(List<?> list) {
        JSONArray jsonArray = new JSONArray();
        if (list == null || list.isEmpty()) {
            return jsonArray;
        }
        for (Object object : list) {
            jsonArray.add(object);
        }
        return jsonArray;
    }

    /**
     *
     * @param json
     * @param c
     * @return java.lang.Object
     * @author liuc
     * @date 2021/11/1 18:29
     * @throws
     */
    public static Object toBean(String json, Class<?> c) {
        return JSON.parseObject(json, c);
    }

    public static Object toBean(byte[] json,Class<?> c){
        return JSON.parseObject(json,c);
    }

    public static Object toBean(String json){
        return JSON.parseObject(json);
    }

    public static List toList(String json, Class<?> c) {
        List list = JSON.parseArray(json, c);
        return list;
    }

    /**
     * Object转成Json格式的String
     * @author liuc
     * @date 2021/11/1 18:25
     * @since JDK1.8
     * @version V1.0
     */
    public static String toJson(Object o){
        return JSON.toJSONString(o);
    }

    /**
     * List转成Json格式的String
     * @param list
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/1 15:47
     * @throws
     */
    public static String toJson(List list){
        return JSON.toJSONString(list);
    }

    public static byte[] toByte(Object o){
        return JSON.toJSONBytes(o);
    }

    /**
     * json转map
     * @param json
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @author liuc
     * @date 2021/11/2 14:51
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static Map<String,Object> jsonToMap(String json) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String,Object> map;
        try {
            map = (Map<String,Object>)objectMapper.readValue(json, Map.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            throw e;
        } catch (JsonMappingException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return map;
    }

    /**
     * json转List<Map<String,?>>
     * @param json
     * @return java.util.List<java.util.Map<java.lang.String,?>>
     * @author liuc
     * @date 2021/11/2 14:49
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static List<Map<String, ?>> jsonToMapList(String json) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, Map.class);
        List<Map<String, ?>> mapList;
        try {
            mapList = (List<Map<String,?>>)objectMapper.readValue(json, javaType);
        } catch (JsonParseException e) {
            e.printStackTrace();
            throw e;
        } catch (JsonMappingException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return mapList;
    }

    /**
     * map转json
     * @param map
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/2 14:49
     * @throws
     */
    public static String mapToJson(Map<String,Object> map) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(map);
        return jsonString;
    }

    /**
     * 对象转json
     * @param object
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/2 14:50
     * @throws
     */
    public static String objectToJson(Object object) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonList;
        try {
            jsonList = objectMapper.writeValueAsString(object);
        } catch (JsonParseException e) {
            e.printStackTrace();
            throw e;
        } catch (JsonMappingException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return jsonList;
    }

    /**
     * List<Map<String,?>>转json
     * @param cardInfoList
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/2 14:50
     * @throws
     */
    public static String mapListToJson(List<Map<String, Object>> cardInfoList) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonList;
        try {
            jsonList = objectMapper.writeValueAsString(cardInfoList);
        } catch (JsonParseException e) {
            e.printStackTrace();
            throw e;
        } catch (JsonMappingException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return jsonList;
    }

    /**
     * 数组转json
     * @param args
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/2 14:50
     * @throws
     */
    public static String arrayToJson(String[] args) throws Exception{
        // 先讲数组转化为map，然后map转json
        Map<String,String> map = new HashMap<String,String>();
        for(int i=0; i<args.length; i++){
            map.put(i+"", args[i]);
        }
        String json = JsonUtil.objectToJson(map);
        return json;
    }

    /**
     * <pre>
     * 对象转化为json字符串
     *
     * @param obj 待转化对象
     * @return 代表该对象的Json字符串
     */
    public static final String toJson(final Object obj, SerializerFeature... features) {
        return JSON.toJSONString(obj, features);
    }

    /**
     * 对象转化为json字符串并格式化
     *
     * @param obj
     * @param format 是否要格式化
     * @return
     */
    public static final String toJson(final Object obj, final boolean format) {
        return JSON.toJSONString(obj, format);
    }

    /**
     * 对象对指定字段进行过滤处理，生成json字符串
     *
     * @param obj
     * @param fields 过滤处理字段
     * @param ignore true做忽略处理，false做包含处理
     * @param features json特征，为null忽略
     * @return
     */
    public static final String toJson(final Object obj, final String[] fields, final boolean ignore,
                                      SerializerFeature... features) {
        if (fields == null || fields.length < 1) {
            return toJson(obj);
        }
        if (features == null) {
            features = new SerializerFeature[] { SerializerFeature.QuoteFieldNames };
        }
        return JSON.toJSONString(obj, new PropertyFilter() {
            @Override
            public boolean apply(Object object, String name, Object value) {
                for (int i = 0; i < fields.length; i++) {
                    if (name.equals(fields[i])) {
                        return !ignore;
                    }
                }
                return ignore;
            }
        }, features);
    }

    /**
     * <pre>
     * 解析json字符串中某路径的值
     *
     * @param json
     * @param path
     * @return
     */
    @SuppressWarnings("unchecked")
    public static final <E> E parse(final String json, final String path) {
        String[] keys = path.split(",");
        JSONObject obj = JSON.parseObject(json);
        for (int i = 0; i < keys.length - 1; i++) {
            obj = obj.getJSONObject(keys[i]);
        }
        return (E) obj.get(keys[keys.length - 1]);
    }

    /**
     * <pre>
     * json字符串解析为对象
     *
     * @param json 代表一个对象的Json字符串
     * @param clazz 指定目标对象的类型，即返回对象的类型
     * @return 从json字符串解析出来的对象
     */
    public static final <T> T parse(final String json, final Class<T> clazz) {
        return JSON.parseObject(json, clazz);
    }

    /**
     * <pre>
     * json字符串解析为对象
     *
     * @param json json字符串
     * @param path 逗号分隔的json层次结构
     * @param clazz 目标类
     */
    public static final <T> T parse(final String json, final String path, final Class<T> clazz) {
        String[] keys = path.split(",");
        JSONObject obj = JSON.parseObject(json);
        for (int i = 0; i < keys.length - 1; i++) {
            obj = obj.getJSONObject(keys[i]);
        }
        String inner = obj.getString(keys[keys.length - 1]);
        return parse(inner, clazz);
    }

    /**
     * 将制定的对象经过字段过滤处理后，解析成为json集合
     *
     * @param obj
     * @param fields
     * @param ignore
     * @param clazz
     * @param features
     * @return
     */
    public static final <T> List<T> parseArray(final Object obj, final String[] fields, boolean ignore,
                                               final Class<T> clazz, final SerializerFeature... features) {
        String json = toJson(obj, fields, ignore, features);
        return parseArray(json, clazz);
    }

    /**
     * <pre>
     * 从json字符串中解析出一个对象的集合，被解析字符串要求是合法的集合类型
     * （形如:["k1":"v1","k2":"v2",..."kn":"vn"]）
     *
     * @param json - [key-value-pair...]
     * @param clazz
     * @return
     */
    public static final <T> List<T> parseArray(final String json, final Class<T> clazz) {
        return JSON.parseArray(json, clazz);
    }

    /**
     * <pre>
     * 从json字符串中按照路径寻找，并解析出一个对象的集合，例如：
     * 类Person有一个属性name，要从以下json中解析出其集合：
     * {
     * "page_info":{
     * "items":{
     * "item":[{"name":"KelvinZ"},{"name":"Jobs"},...{"name":"Gates"}]
     * }
     * }
     * 使用方法：parseArray(json, "page_info,items,item", Person.class)，
     * 将根据指定路径，正确的解析出所需集合，排除外层干扰
     *
     * @param json json字符串
     * @param path 逗号分隔的json层次结构
     * @param clazz 目标类
     * @return
     */
    public static final <T> List<T> parseArray(final String json, final String path, final Class<T> clazz) {
        String[] keys = path.split(",");
        JSONObject obj = JSON.parseObject(json);
        for (int i = 0; i < keys.length - 1; i++) {
            obj = obj.getJSONObject(keys[i]);
        }
        String inner = obj.getString(keys[keys.length - 1]);
        List<T> ret = parseArray(inner, clazz);
        return ret;
    }

    /**
     * <pre>
     * 有些json的常见格式错误这里可以处理，以便给后续的方法处理
     * 常见错误：使用了\" 或者 "{ 或者 }"，腾讯的页面中常见这种格式
     *
     * @param invalidJson 包含非法格式的json字符串
     * @return
     */
    public static final String correctJson(final String invalidJson) {
        String content = invalidJson.replace("\\\"", "\"").replace("\"{", "{").replace("}\"", "}");
        return content;
    }

    /**
     * 格式化Json
     *
     * @param json
     * @return
     */
    public static final String formatJson(String json) {
        Map<?, ?> map = (Map<?, ?>) JSON.parse(json);
        return JSON.toJSONString(map, true);
    }

    /**
     * 获取json串中的子json
     *
     * @param json
     * @param path
     * @return
     */
    public static final String getSubJson(String json, String path) {
        String[] keys = path.split(",");
        JSONObject obj = JSON.parseObject(json);
        for (int i = 0; i < keys.length - 1; i++) {
            obj = obj.getJSONObject(keys[i]);
            log.info(obj.toJSONString());
        }
        return obj != null ? obj.getString(keys[keys.length - 1]) : null;
    }
}
