package com.buckwheat.common.exception;

/**
 * 系统错误码
 * @author liuc
 * @date 2021/12/4 15:48
 * @since JDK1.8
 * @version V1.0
 */
public enum BusinessErrorCode {
    /**
     * 错误：{0}
     */
    CSRCB99999("CSRCB99999","错误：{0}",1),
    /**
     * 数据库查询异常,异常原因:{0}
     */
    CSRCB10001("CSRCB10001","数据库查询异常,异常原因:{0}",1),
    /**
     * 数据库删除异常,异常原因:{0}
     */
    CSRCB10002("CSRCB10002","数据库删除异常,异常原因:{0}",1),
    /**
     * 数据库修改异常,异常原因:{0}
     */
    CSRCB10003("CSRCB10003","数据库修改异常,异常原因:{0}",1),
    /**
     * 数据库插入异常,异常原因:{0}
     */
    CSRCB10004("CSRCB10004","数据库插入异常,异常原因:{0}",1),
    /**
     * 错误：查询【{0}】结果为空
     */
    CSRCB10005("CSRCB10005","错误：查询【{0}】结果为空",1),
    /**
     * 【{0}】时间格式转换异常
     */
    CSRCB10006("CSRCB10006","【{0}】时间格式转换异常",1),
    /**
     * 错误:参数【{0}】不能为空
     */
    CSRCB30000("CSRCB30000", "错误:参数【{0}】不能为空", 1),
    /**
     * 错误:报文头请求参数【{0}】不能为空
     */
    CSRCB30001("CSRCB30001", "错误:报文头请求参数【{0}】不能为空", 1),
    /**
     * 【{0}】的码值【{1}】不在标准码范围内
     */
    CSRCB30002("CSRCB30002", "【{0}】的码值【{1}】不在标准码范围内", 1),
    /**
     * 【{0}】必须为yyyyMMdd格式
     */
    CSRCB30003("CSRCB30003", "【{0}】必须为yyyyMMdd格式", 1),
    /**
     * 【{0}】必须为HH24MISS格式
     */
    CSRCB30004("CSRCB30004", "【{0}】必须为HH24MISS格式", 1),
    /**
     * 【{0}】不存在
     */
    CSRCB30005("CSRCB30005", "【{0}】不存在", 1),
    /**
     * 参数不能为空
     */
    CSRCB30006("CSRCB30006", "参数不能为空", 1),
    /**
     * 错误:调用服务【{0}】失败：【{1}】
     */
    CSRCB30007("CSRCB30007", "错误:调用服务【{0}】失败：【{1}】", 1),
    /**
     * 暂不支持的输入参数:{0}为{1}
     */
    CSRCB30008("CSRCB30008", "暂不支持的输入参数:{0}为{1}", 1),
    /**
     * 未找到资源文件:{0}
     */
    CSRCB30009("CSRCB30009", "未找到资源文件:{0}", 1),

    SUCCESS("000000", "交易成功", 0),

    BUSINESS_FAILD("100000", "业务执行失败", 2),

    NON_DATA("100001", "操作失败，数据不存在", 1),

    DATA_REPETITION("100002", "已存在相同的数据,操作失败", 1),

    FREQUENTLY("100003", "操作过于频繁", 1),

    TRANS_UNKNOW("100004", "交易状态未知,需查证", 3),

    RQEUIRED_LOGIN("200000", "您需要先登录才能执行此操作", 1),

    UN_AUTHORIZATION("200002", "您没有权限执行此操作", 1),

    VALIDATE_FAILD("500000", "请求参数验证失败", 1),

    NON_INTERFACE("500002", "请求的接口不存在", 1),

    REQ_TIMEOUT("600000", "系统繁忙，请稍后再试....", 2),

    NETWORK_FAIL("600001", "服务器内部通讯失败", 1),

    UNKNOW_FAIL("999999", "请求执行失败(未知错误),请联系管理员", 4);


    private String code;
    private int level;
    private String msg;

    public String getCode() { return this.code; }

    public String getMsg()
    {
        return this.msg;
    }

    public int getLevel() {
        return this.level;
    }

    private BusinessErrorCode(String code, String msg, int level) {
        this.code = code;
        this.msg = msg;
        this.level = level;
    }

    public static BusinessErrorCode parse(String code) {
        BusinessErrorCode[] codes = values();
        for (BusinessErrorCode c : codes) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return UNKNOW_FAIL;
    }

}
