package com.buckwheat.common.advice;

import com.buckwheat.common.exception.BusinessErrorCode;
import com.buckwheat.common.exception.BusinessException;
import com.buckwheat.common.message.RequestInfo;
import com.buckwheat.common.message.ResponseBuilder;
import com.buckwheat.common.message.ResponseInfo;
import com.buckwheat.common.util.JsonUtil;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.util.ContentCachingRequestWrapper;
import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * 统一异常处理器
 * @author liuc
 * @version V1.0
 * @date 2021/12/3 15:07
 * @since JDK1.8
 */
@RestControllerAdvice
@ResponseBody
@Log4j2
public class ExceptionHandlerAdvice {
    @Resource
    private ResponseBuilder responseBuilder;

    /**
     * 处理未捕获的Exception
     * @param e 异常
     * @param request 请求信息
     * @return com.buckwheat.common.message.ResponseInfo 统一响应体
     * @author liuc
     * @date 2021/12/4 16:01
     * @throws
     */
    @SneakyThrows
    @ExceptionHandler(Exception.class)
    public ResponseInfo handleException(Exception e,ServletRequest request){
        Map<String,Object> reqMap = new HashMap<String,Object>(8);
        RequestInfo requestInfo = null;
        if (request != null && request instanceof ContentCachingRequestWrapper) {
            ContentCachingRequestWrapper wrapper = (ContentCachingRequestWrapper) request;
            requestInfo = JsonUtil.stringToObj(StringUtils.toEncodedString(wrapper.getContentAsByteArray(),
                    Charset.forName(wrapper.getCharacterEncoding())),RequestInfo.class);
        }
        ResponseInfo responseInfo = responseBuilder.buildResponse(requestInfo);
        responseInfo.setError(BusinessErrorCode.UNKNOW_FAIL.getCode(),e.getMessage());
        return responseInfo;
    }

    /**
     * 处理未捕获的RuntimeException
     * @param e 运行时异常
     * @return 统一响应体
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseInfo handleRuntimeException(RuntimeException e,ServletRequest request){
        Map<String,Object> reqMap = new HashMap<String,Object>(8);
        RequestInfo requestInfo = null;
        if (request != null && request instanceof ContentCachingRequestWrapper) {
            ContentCachingRequestWrapper wrapper = (ContentCachingRequestWrapper) request;
            requestInfo = JsonUtil.stringToObj(StringUtils.toEncodedString(wrapper.getContentAsByteArray(),
                    Charset.forName(wrapper.getCharacterEncoding())),RequestInfo.class);
        }
        ResponseInfo responseInfo = responseBuilder.buildResponse(requestInfo);
        responseInfo.setError(BusinessErrorCode.UNKNOW_FAIL.getCode(),BusinessErrorCode.UNKNOW_FAIL.getMsg());
        return responseInfo;
    }

    /**
     * 处理业务异常
     * @param e 业务异常
     * @param request
     * @return com.buckwheat.common.message.ResponseInfo 统一响应体
     * @author liuc
     * @date 2021/12/4 16:02
     * @throws
     */
    @ExceptionHandler(BusinessException.class)
    public ResponseInfo handleBaseException(BusinessException e,ServletRequest request){
        Map<String,Object> reqMap = new HashMap<String,Object>(8);
        RequestInfo requestInfo = null;
        if (request != null && request instanceof ContentCachingRequestWrapper) {
            ContentCachingRequestWrapper wrapper = (ContentCachingRequestWrapper) request;
            requestInfo = JsonUtil.stringToObj(StringUtils.toEncodedString(wrapper.getContentAsByteArray(),
                    Charset.forName(wrapper.getCharacterEncoding())),RequestInfo.class);
        }
        ResponseInfo responseInfo = responseBuilder.buildResponse(requestInfo);
        responseInfo.setError(BusinessErrorCode.CSRCB99999.getCode(),e.getMessage());
        return responseInfo;
    }
}