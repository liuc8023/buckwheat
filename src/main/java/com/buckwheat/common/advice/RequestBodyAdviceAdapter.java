package com.buckwheat.common.advice;

import lombok.extern.log4j.Log4j2;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
import java.io.IOException;
import java.lang.reflect.Type;

/**
 * 统一请求信息处理器
 * @author liuc
 * @version V1.0
 * @date 2021/12/4 13:49
 * @since JDK1.8
 */
@RestControllerAdvice
@Log4j2
public class RequestBodyAdviceAdapter implements RequestBodyAdvice {

    /**
     * 该方法用于判断当前请求，是否要执行beforeBodyRead方法
     * @param methodParameter 方法的参数对象
     * @param targetType 方法的参数类型
     * @param converterType 将会使用到的Http消息转换器类类型
     * @return boolean 返回true则会执行beforeBodyRead
     * @author liuc
     * @date 2021/12/4 13:57
     * @throws
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return false;
    }

    /**
     * 在Http消息转换器执转换，之前执行
     * @param inputMessage 客户端的请求数据
     * @param parameter 方法的参数对象
     * @param targetType 方法的参数类型
     * @param converterType 将会使用到的Http消息转换器类类型
     * @return org.springframework.http.HttpInputMessage 返回 一个自定义的HttpInputMessage
     * @author liuc
     * @date 2021/12/4 13:58
     * @throws
     */
    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
        return inputMessage;
    }

    /**
     * 在Http消息转换器执转换，之后执行
     * @param body 转换后的对象
     * @param inputMessage 客户端的请求数据
     * @param parameter 方法的参数类型
     * @param targetType 方法的参数类型
     * @param converterType 使用的Http消息转换器类类型
     * @return java.lang.Object 返回一个新的对象
     * @author liuc
     * @date 2021/12/4 13:59
     * @throws
     */
    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }

    /**
     * 在Http消息转换器执转换，之后执行，同上，不过这个方法处理的是，body为空的情况
     * @param body 转换后的对象
     * @param inputMessage 客户端的请求数据
     * @param parameter 方法的参数类型
     * @param targetType 方法的参数类型
     * @param converterType 使用的Http消息转换器类类型
     * @return java.lang.Object 返回一个新的对象
     * @author liuc
     * @date 2021/12/4 13:59
     * @throws
     */
    @Override
    public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }
}
