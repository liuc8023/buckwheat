package com.buckwheat.common.advice;

import com.buckwheat.common.message.RequestInfo;
import com.buckwheat.common.message.ResponseBuilder;
import com.buckwheat.common.message.ResponseInfo;
import com.buckwheat.common.util.JsonUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import org.springframework.web.util.ContentCachingRequestWrapper;
import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.nio.charset.Charset;

/**
 * 统一响应体处理器
 * @author liuc
 * @version V1.0
 * @date 2021/12/3 15:06
 * @since JDK1.8
 */
@RestControllerAdvice
@Log4j2
public class ResponseHandlerAdviceAdapter implements ResponseBodyAdvice {
    @Resource
    private ResponseBuilder responseBuilder;

    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        log.info("returnType:"+returnType);
        log.info("converterType:"+converterType);
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        // 此处获取到request 为特殊需要的时候处理使用
        ServletRequest request = ((ServletServerHttpRequest) serverHttpRequest).getServletRequest();
        // 判断响应的Content-Type为JSON格式的body
        log.info("selectedContentType:"+selectedContentType);
        ResponseInfo responseInfo = null;
        if(MediaType.APPLICATION_JSON.equals(selectedContentType) || MediaType.TEXT_PLAIN.equals(selectedContentType)){
            // 如果响应返回的对象为统一响应体，则直接返回body
            if(body instanceof ResponseInfo){
                responseInfo = (ResponseInfo) body;
            }else{
                // 只有正常返回的结果才会进入这个判断流程，所以返回正常成功的状态码
                RequestInfo requestInfo = null;
                if (request != null && request instanceof ContentCachingRequestWrapper) {
                    //从ContentCachingRequestWrapper调用链中获取请求信息
                    ContentCachingRequestWrapper wrapper = (ContentCachingRequestWrapper) request;
                    requestInfo = JsonUtil.stringToObj(StringUtils.toEncodedString(wrapper.getContentAsByteArray(),
                            Charset.forName(wrapper.getCharacterEncoding())),RequestInfo.class);
                    responseInfo = responseBuilder.buildResponse(requestInfo);
                    responseInfo.setResponseBody(body);
                }

            }
        }
        log.info("服务[{}]响应数据:{}" ,returnType.getMethod().getName(), JsonUtil.toJson(responseInfo));
        return responseInfo;
    }
}