package com.buckwheat.common.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/11/15 11:20
 * @since JDK1.8
 */
// 该注解用于类上
@Target({ElementType.TYPE})
// 在运行时起作用
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface InitDataSource {
    String[] value();
}
