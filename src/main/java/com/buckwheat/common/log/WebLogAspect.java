package com.buckwheat.common.log;

import com.buckwheat.common.message.RequestInfo;
import com.buckwheat.common.util.JsonUtil;
import com.buckwheat.common.util.UtilValidate;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NamedThreadLocal;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/11/25 15:35
 * @since JDK1.8
 */
@Aspect
@Order(5)
@Component
@Log4j2
public class WebLogAspect {
    ThreadLocal<String> threadLocal = new ThreadLocal<>();
    private ThreadLocal<StopWatch> stopWatchThreadLocal = new ThreadLocal<>();
    public final static String GUID = "guid";
    public final static String REQUEST_GUID = "requestGuid";
    /**
     * GUID ThreadLocal 存储当前线程的guid
     */
    public static final ThreadLocal<String> GuidhreadLocal = new NamedThreadLocal<String>("GuidhreadLocal");

    /**
     * 通过自动装配 拿到request及response
     * 切点定义的不同，可能导致自动装配失败，这里定义required=false，忽略无法装配的情况
     */
    @Autowired(required=false)
    HttpServletRequest request;
    @Autowired(required=false)
    HttpServletResponse response;

    public static void init(){
        String guid = UUID.randomUUID().toString().replace("-", "");
        GuidhreadLocal.set(guid);
        MDC.put(GUID, guid);
    }


    /**
     * 切点
     */
    @Pointcut("execution(public * com.buckwheat..*Controller.*(..))")
    public void controllerLog() {
    }

    @Before("controllerLog()")
    public void doBefore(JoinPoint joinPoint){
        dealGuid(joinPoint);
        //开始计时
        StopWatch sw = new StopWatch();
        stopWatchThreadLocal.set(sw);
        sw.start();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //打印请求的内容
        //获取请求头中的User-Agent
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        log.info("接口路径：{}" , request.getRequestURL().toString());
        log.info("浏览器：{}", userAgent.getBrowser().toString());
        log.info("浏览器版本：{}",userAgent.getBrowserVersion());
        log.info("操作系统: {}", userAgent.getOperatingSystem().toString());
        log.info("IP : {}" , request.getRemoteAddr());
        log.info("请求类型：{}", request.getMethod());
        log.info("类方法 : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("服务[{}]请求数据:{}",joinPoint.getSignature().getName(), JsonUtil.toJson(joinPoint.getArgs()));
    }

    @Around("controllerLog()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        dealGuid(joinPoint);
        Object result = joinPoint.proceed();
        StopWatch sw = stopWatchThreadLocal.get();
        sw.stop();
        long timeConsuming = sw.getTotalTimeMillis();
        //处理完请求后，返回内容
        log.info("服务[{}]执行耗时:{}ms" ,joinPoint.getSignature().getName(),timeConsuming);
        return result;
    }

    @AfterReturning("controllerLog()")
    public void doAfterReturning(JoinPoint joinPoint){
        dealGuid(joinPoint);
    }

    @After("controllerLog()")
    public void doAfter(JoinPoint joinPoint) {
        dealGuid(joinPoint);
    }

    /**
     *  异常通知 即使出现错误，也不要丢了guid信息
     * @param joinPoint
     * @param e
     */
    @AfterThrowing(pointcut = "controllerLog()", throwing = "e")
    public  void doAfterThrowing(JoinPoint joinPoint, Throwable e) {
        dealGuid(joinPoint);
    }

    public void dealGuid(JoinPoint joinPoint){
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        ContentCachingRequestWrapper wrapper = (ContentCachingRequestWrapper) request;
        RequestInfo requestInfo = JsonUtil.stringToObj(StringUtils.toEncodedString(wrapper.getContentAsByteArray(),
                Charset.forName(wrapper.getCharacterEncoding())),RequestInfo.class);
        // 依次从 request threadlocal 里取
        String guid = GuidhreadLocal.get();
        if(StringUtils.isEmpty(guid) && request != null) {
            guid = requestInfo.getRequestHead().getOrgConsumerSeqNo();
        }
        // 无法读取有效的guid重新生成
        if(StringUtils.isEmpty(guid)){
            guid = UUID.randomUUID().toString().replace("-", "");
        }
        // 设置SessionId
        GuidhreadLocal.set(guid);
        if(response != null) {
            response.setHeader(REQUEST_GUID, guid);
        }
        MDC.put(GUID, guid);
    }

}
