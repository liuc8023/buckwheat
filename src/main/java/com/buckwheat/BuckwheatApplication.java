package com.buckwheat;

import com.buckwheat.module.common.lock.config.DistributedLockConfig;
import com.buckwheat.module.common.lock.factory.DistributedLockFactory;
import com.buckwheat.module.common.lock.impl.IDistributedLock;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/9/28 20:35
 * @since JDK1.8
 */
@SpringBootApplication(scanBasePackages = {"com.buckwheat"},exclude={DataSourceAutoConfiguration.class})
@RestController
@Log4j2
public class BuckwheatApplication {
    @Resource
    DistributedLockFactory factory;
    @Resource
    DistributedLockConfig config;
    private static final Logger logger = LoggerFactory.getLogger(BuckwheatApplication.class);
    public static final String MODULE = BuckwheatApplication.class.getName();
    public static void main(String[] args) {
        SpringApplication.run(BuckwheatApplication.class, args);
    }

    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        //使用分布式锁
        IDistributedLock lock = factory.getDistributedLock(config.getType());
        String lockName = "234234_7777777" ;
        boolean isLock = lock.getExclusiveLock(lockName);
        if (!isLock) {
            logger.info(111111 + "，请勿重复提交!", MODULE);
            return  "请勿重复提交!";
        }
        String returnStr = null;
        try{
            returnStr = String.format("Hello %s!", name);
            try {
                TimeUnit.SECONDS.sleep(6);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }finally {
            if (lock.isLock(lockName)) {
                //释放锁
                lock.unLock(lockName);
            }
        }
        return returnStr;
    }
}
